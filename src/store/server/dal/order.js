import moment from "moment"
import Customer from "../models/customers"
import Order from "../models/order"
import Product from "../models/product"
import TimeLine from "../models/timeline"

const population = [
  {
    path: "customer",
    model: Customer,
  },
  {
    path: "product",
    model: Product,
  },
  {
    path: "timeline",
    model: TimeLine,
  },
]

class OrderDal {
  constructor() {}

  static get(query, cb) {
    Order.findOne(query)
      .populate(population)
      .exec((err, doc) => {
        if (err) {
          return cb(err)
        }

        cb(null, doc || {})
      })
  }

  static create(OrderData, cb) {
    const searchQuery = {
      username: OrderData.username,
    }
    const OrderModel = new Order(OrderData)

    OrderModel.save(function SaveOrder(err, data) {
      if (err) {
        return cb(err)
      }

      OrderDal.get({ _id: data._id }, (err, doc) => {
        if (err) {
          return cb(err)
        }

        cb(null, doc)
      })
    })
  }

  static delete(query, cb) {
    Order.findOne(query)
      .populate(population)
      .exec(function deleteOrder(err, doc) {
        if (err) {
          return cb(err)
        }

        if (!doc) {
          return cb(null, {})
        }

        Order.remove(query, err => {
          if (err) {
            return cb(err)
          }

          cb(null, doc)
        })
      })
  }

  static update(query, updates, cb) {
    const now = moment().toISOString()

    Order.findOneAndUpdate(query, updates, {}).exec((err, cust) => {
      if (err) {
        return cb(err)
      }
      cb(null, cust || {})
    })
  }

  static getCollection(query, qs, cb) {
    Order.find(query, {}, qs)
      .sort({ _id: -1 })
      .populate(population)
      .exec(function getOrderCollection(err, doc) {
        if (err) {
          return cb(err)
        }

        return cb(null, doc)
      })
  }
}

export default OrderDal
